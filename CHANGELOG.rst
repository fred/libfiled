ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

2.0.0 (2024-09-11)
------------------

* ``Fileman`` gRPC API updated to version ``1.2.0``

1.1.0 (2022-06-22)
------------------

* Function `read` to read the file from fileman

1.0.0 (2022-01-27)
------------------

* Initial ``LibFiled`` implementation
