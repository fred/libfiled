/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPE_HH_5B29CD4A55D5495ABEC4682CD2F6702B
#define STRONG_TYPE_HH_5B29CD4A55D5495ABEC4682CD2F6702B

#include "libstrong/type.hh"

#include <string>

namespace LibFiled {

namespace StrongTypeSkill = LibStrong::Skill;

template <typename Type, typename Tag, template <typename> class ...Skills>
using StrongType = LibStrong::TypeWithSkills<Type, Tag, Skills...>;

template <typename StrongType>
using Optional = LibStrong::Optional<StrongType>;

template <typename Tag>
using StrongString = StrongType<std::string, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

} // namespace LibFiled

#endif
