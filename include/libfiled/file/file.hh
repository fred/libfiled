/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILE_HH_5826FA63D424437296D6016975840C93
#define FILE_HH_5826FA63D424437296D6016975840C93

#include "libfiled/strong_type.hh"

namespace LibFiled {
namespace File {

using FileName = StrongString<struct FileNameTag_>;
using FileMimeType = StrongString<struct FileMimeTypeTag_>;
using FileUid = StrongString<struct FileUidTag_>;

} // namespace LibFiled::File
} // namespace LibFiled

#endif
