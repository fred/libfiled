/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_HH_04C7337BA10E44FC824173CA92CB5566
#define CREATE_HH_04C7337BA10E44FC824173CA92CB5566

#include "libfiled/connection.hh"
#include "libfiled/exception.hh"
#include "libfiled/file/file.hh"
#include "libfiled/service.hh"

#include <istream>

namespace LibFiled {
namespace File {

class CreateFailed : public LibFiled::GrpcException
{
    using LibFiled::GrpcException::GrpcException;
};

FileUid create(
        const Connection<Service::File>& _connection,
        const FileName& _file_name,
        std::istream& _file_data,
        const Optional<FileMimeType>& _file_mime_type);

} // namespace LibFiled::File
} // namespace LibFiled

#endif
