/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_HH_187ACF29AAF2438FA2DE39BE30AA3BB5
#define CONNECTION_HH_187ACF29AAF2438FA2DE39BE30AA3BB5

#include "libfiled/service.hh"
#include "libfiled/strong_type.hh"

#include <memory>

namespace LibFiled {

class RpcStubAlias;

template <typename S>
class Connection
{
public:
    class Accessor;

    using ConnectionString = StrongString<struct ConnectionStringTag_>;

    explicit Connection(const ConnectionString& _connection_string);

    ~Connection();

private:
    struct RpcStubDeleter
    {
        void operator()(RpcStubAlias*) const;
    };

    std::unique_ptr<RpcStubAlias, RpcStubDeleter> rpc_stub_wrapper_;
};

extern template class Connection<Service::File>;

} // namespace LibFiled

#endif
