/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_STUB_HH_7F11D955DD0D47EC8015113510894A63
#define GET_STUB_HH_7F11D955DD0D47EC8015113510894A63

#include "include/libfiled/connection.hh"

#include "src/connection_accessor.hh"

namespace LibFiled {

template <typename S>
auto& get_stub(const Connection<S>& _connection)
{
    return Connection<S>::Accessor::get_stub(_connection);
}

} // namespace LibFiled

#endif
