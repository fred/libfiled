/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_ACCESSOR_HH_69966F91EFE74B159BBECD774F3E90CD
#define CONNECTION_ACCESSOR_HH_69966F91EFE74B159BBECD774F3E90CD

#include "include/libfiled/connection.hh"

#include "src/grpc_type_traits.hh"

#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

namespace LibFiled {

template <typename S>
class Connection<S>::Accessor
{
public:
    static GrpcStubType<S>& get_stub(const Connection<S>& _connection);
    static std::unique_ptr<RpcStubAlias, RpcStubDeleter> wrap_stub(const ConnectionString& _connection_string);
private:
    static std::unique_ptr<GrpcStubType<S>> make_stub(const ConnectionString& _connection_string);
};

template <typename S>
GrpcStubType<S>& Connection<S>::Accessor::get_stub(const Connection<S>& _connection)
{
    return *reinterpret_cast<GrpcStubType<S>*>(_connection.rpc_stub_wrapper_.get());
}

template <typename S>
auto Connection<S>::Accessor::wrap_stub(const ConnectionString& _connection_string) -> std::unique_ptr<RpcStubAlias, RpcStubDeleter>
{
    return std::unique_ptr<RpcStubAlias, RpcStubDeleter>{reinterpret_cast<RpcStubAlias*>(make_stub(_connection_string).release())};
}

template <typename S>
std::unique_ptr<GrpcStubType<S>> Connection<S>::Accessor::make_stub(const ConnectionString& _connection_string)
{
    const auto channel = grpc::CreateChannel(*_connection_string, grpc::InsecureChannelCredentials());
    return GrpcServiceType<S>::NewStub(channel);
}

} // namespace LibFiled

#endif
