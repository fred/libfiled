/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/file/proto_unwrap.hh"

#include "include/libfiled/file/file.hh"

#include <ostream>

namespace LibFiled {
namespace File {

namespace {

FileUid proto_unwrap(const Fred::Fileman::Api::Uid& _src)
{
    return FileUid{_src.value()};
}

} // namespace LibFiled::File::{anonymous}

CreateReply proto_unwrap(const Fred::Fileman::Api::CreateReply& _src)
{
    return CreateReply{proto_unwrap(_src.data().uid())};
}

void proto_unwrap(const Fred::Fileman::Api::ReadReply& _src, ReadReply& _dst)
{
    _dst.file_data << _src.data().data();
}

} // namespace LibFiled::File
} // namespace LibFiled
