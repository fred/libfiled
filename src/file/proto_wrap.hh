/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_WRAP_HH_2D6FDE7D3DAC49EEB58BCF127A460EF1
#define PROTO_WRAP_HH_2D6FDE7D3DAC49EEB58BCF127A460EF1

#include "src/file/create_request.hh"
#include "src/file/read_request.hh"

#include "fred_api/fileman/service_fileman_grpc.pb.h"

namespace LibFiled {
namespace File {

template <typename>
struct InternalTypeTraits
{
    struct ProtoType;
};

template <>
struct InternalTypeTraits<CreateRequest>
{
    using ProtoType = Fred::Fileman::Api::CreateRequest;
};

template <>
struct InternalTypeTraits<ReadRequest>
{
    using ProtoType = Fred::Fileman::Api::ReadRequest;
};

void proto_wrap(
        const CreateRequest& _src,
        CreateRequestDataChunk& _src_data,
        InternalTypeTraits<CreateRequest>::ProtoType* _dst);

void proto_wrap(
        const ReadRequest& _src,
        InternalTypeTraits<ReadRequest>::ProtoType* _dst);

} // namespace LibFiled::File
} // namespace LibFiled

#endif
