/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_UNWRAP_HH_9EC4B0C058C0455AAD1B7DADA1A38A8F
#define PROTO_UNWRAP_HH_9EC4B0C058C0455AAD1B7DADA1A38A8F

#include "src/file/create_reply.hh"
#include "src/file/read_reply.hh"

#include "fred_api/fileman/service_fileman_grpc.pb.h"

namespace LibFiled {
namespace File {

CreateReply proto_unwrap(const Fred::Fileman::Api::CreateReply& _src);

void proto_unwrap(const Fred::Fileman::Api::ReadReply& _src, ReadReply& _dst);

} // namespace LibFiled::File
} // namespace LibFiled

#endif
