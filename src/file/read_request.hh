/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef READ_REQUEST_HH_CBE797E63BD44818B0F66A1A103C4BC4
#define READ_REQUEST_HH_CBE797E63BD44818B0F66A1A103C4BC4

#include "include/libfiled/file/read.hh"

#include "libstrong/type.hh"

#include <cstddef>

namespace LibFiled {
namespace File {

using ChunkSizeMax = LibStrong::Type<std::size_t, struct ChunkSizeMaxTag_>;

struct ReadRequest
{
    FileUid file_uid;
    ChunkSizeMax chunk_size_max;
};


} // namespace LibFiled::File
} // namespace LibFiled

#endif
