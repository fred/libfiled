/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libfiled/file/read.hh"

#include "include/libfiled/exception.hh"

#include "src/file/read_reply.hh"
#include "src/file/read_request.hh"
#include "src/file/proto_unwrap.hh"
#include "src/file/proto_wrap.hh"
#include "src/get_stub.hh"

#include "fred_api/fileman/service_fileman_grpc.pb.h"

#include <grpcpp/client_context.h>

namespace LibFiled {
namespace File {

namespace Api = Fred::Fileman::Api;

void read(
        const Connection<Service::File>& _connection,
        const FileUid& _file_uid,
        std::ostream& _file_data)
{
    static constexpr std::size_t max_send_message_length{4 * 1024 * 1024};
    static constexpr std::size_t chunk_size_max{max_send_message_length / 2};

    const ReadRequest read_request{_file_uid, ChunkSizeMax{chunk_size_max}};
    Api::ReadRequest api_read_request;
    proto_wrap(read_request, &api_read_request);

    grpc::ClientContext context;
    const auto reader = get_stub(_connection).read(&context, api_read_request);

    Api::ReadReply api_read_reply;
    ReadReply read_reply{_file_data};
    while (reader->Read(&api_read_reply))
    {
        proto_unwrap(api_read_reply, read_reply);
    }

    const auto status = reader->Finish();

    if (!status.ok())
    {
        throw ReadFailed(status.error_code(), status.error_message());
    }
}

} // namespace LibFiled::File
} // namespace LibFiled
