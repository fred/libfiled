/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/file/proto_wrap.hh"

namespace LibFiled {
namespace File {

void proto_wrap(const FileUid& _src, Fred::Fileman::Api::Uid* _dst)
{
    _dst->set_value(*_src);
}

void proto_wrap(
        const CreateRequest& _src,
        CreateRequestDataChunk& _src_data,
        InternalTypeTraits<CreateRequest>::ProtoType* _dst)
{
    _dst->set_name(*_src.file_name);
    {
        std::string strbuff;
        strbuff.resize(_src_data.file_data_chunk_size);
        _src_data.file_data.read(&strbuff[0], _src_data.file_data_chunk_size);
        if (!_src_data.file_data)
        {
            strbuff.resize(_src_data.file_data.gcount());
        }
        _dst->set_data(std::move(strbuff));
    }
    if (has_value(_src.file_mime_type))
    {
        _dst->set_mimetype(**_src.file_mime_type);
    }
}

void proto_wrap(
        const ReadRequest& _src,
        InternalTypeTraits<ReadRequest>::ProtoType* _dst)
{
    proto_wrap(_src.file_uid, _dst->mutable_uid());
    _dst->set_size(*_src.chunk_size_max);
}

} // namespace LibFiled::File
} // namespace LibFiled
