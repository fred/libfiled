/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_REQUEST_HH_0A98909D74674FA5838EE66FA36AB1FA
#define CREATE_REQUEST_HH_0A98909D74674FA5838EE66FA36AB1FA

#include "include/libfiled/file/create.hh"

namespace LibFiled {
namespace File {

struct CreateRequest
{
    FileName file_name;
    Optional<FileMimeType> file_mime_type;
};

struct CreateRequestDataChunk
{
    CreateRequestDataChunk(
            std::istream& _file_data_chunk,
            std::size_t _file_data_chunk_size);

    std::istream& file_data;
    std::size_t file_data_chunk_size;
};

} // namespace LibFiled::File
} // namespace LibFiled

#endif
