/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libfiled/file/create.hh"

#include "include/libfiled/exception.hh"

#include "src/file/create_reply.hh"
#include "src/file/create_request.hh"
#include "src/file/proto_unwrap.hh"
#include "src/file/proto_wrap.hh"
#include "src/get_stub.hh"

#include "fred_api/fileman/service_fileman_grpc.pb.h"

#include <grpcpp/client_context.h>

namespace LibFiled {
namespace File {

namespace Api = Fred::Fileman::Api;

CreateRequestDataChunk::CreateRequestDataChunk(
        std::istream& _file_data_chunk,
        std::size_t _file_data_chunk_size)
    : file_data(_file_data_chunk),
      file_data_chunk_size(_file_data_chunk_size)
{
}

FileUid create(
        const Connection<Service::File>& _connection,
        const FileName& _file_name,
        std::istream& _file_data,
        const Optional<FileMimeType>& _file_mime_type)
{
    static constexpr std::size_t max_send_message_length{4 * 1024 * 1024};
    static constexpr std::size_t chunk_size_max{max_send_message_length / 2};

    {
        CreateRequestDataChunk create_request_data_chunk(_file_data, 0);
        Api::CreateRequest create_request;
        proto_wrap(CreateRequest{_file_name, _file_mime_type}, create_request_data_chunk, &create_request);
        std::string buffer;
        create_request.SerializeToString(&buffer);
        const auto size_of_request_without_data = buffer.size();
        if (size_of_request_without_data > (max_send_message_length - chunk_size_max))
        {
            throw Exception{"message metadata too long"};
        }
    }

    grpc::ClientContext context;
    Api::CreateRequest create_request;
    Api::CreateReply create_reply;

    const auto writer = get_stub(_connection).create(&context, &create_reply);

    CreateRequestDataChunk create_request_data_chunk(_file_data, chunk_size_max);
    bool first_chunk{true};
    do
    {
        proto_wrap(
                first_chunk ? CreateRequest{_file_name, _file_mime_type}
                            : CreateRequest{FileName{""}, FileMimeType::nullopt},
                create_request_data_chunk,
                &create_request);

        const auto stream_ok =
                create_request_data_chunk.file_data
                        ? writer->Write(create_request)
                        : writer->Write(create_request, grpc::WriteOptions{}.set_last_message());

        if (!stream_ok)
        {
            throw Exception{"bad gRPC stream"};
        }
        first_chunk = false;
    }
    while (create_request_data_chunk.file_data);

    const auto status = writer->Finish();
    if (!status.ok())
    {
        throw CreateFailed(status.error_code(), status.error_message());
    }

    return proto_unwrap(create_reply).uid;
}

} // namespace LibFiled::File
} // namespace LibFiled
