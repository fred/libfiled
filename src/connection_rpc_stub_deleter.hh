/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_RPC_STUB_DELETER_HH_AC45F25E374649749FCA113EC0574DF7
#define CONNECTION_RPC_STUB_DELETER_HH_AC45F25E374649749FCA113EC0574DF7

#include "include/libfiled/connection.hh"

#include "src/grpc_type_traits.hh"

namespace LibFiled {

template <typename S>
void Connection<S>::RpcStubDeleter::operator()(RpcStubAlias* rpc_stub_alias) const
{
    using Deleter = typename std::unique_ptr<GrpcStubType<S>>::deleter_type;
    Deleter{}(reinterpret_cast<GrpcStubType<S>*>(rpc_stub_alias));
}

} // namespace LibFiled

#endif
