/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libfiled/file/create.hh"
#include "libfiled/file/read.hh"

#include <cstdlib>
#include <iostream>
#include <sstream>

int main()
{
    LibFiled::Connection<LibFiled::Service::File> connection{
        LibFiled::Connection<LibFiled::Service::File>::ConnectionString{
            "localhost:50051"}};

    std::istringstream input_data(std::string{"12345678-1234-1234-1234-123456789012 line2"});
    std::ostringstream output_data;

    try
    {
        const auto file_uid =
                LibFiled::File::create(
                        connection,
                        LibFiled::File::FileName{"file_name.txt"},
                        input_data,
                        LibFiled::File::FileMimeType{"text/plain"});

        std::cout << "File UID: " << file_uid << std::endl;

        LibFiled::File::read(
                connection,
                file_uid,
                output_data);

        std::cout << "File Content: " << output_data.str() << std::endl;
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::cerr << "std::exception caught while creating file: " << e.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "std::exception caught while creating file" << std::endl;
    }
    return EXIT_FAILURE;
}
